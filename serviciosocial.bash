#!/bin/bash
# Description (es):
#   Lista las vacantes por perfil de servicio social
#   para la carrera de Ingeniería en sistemas computaciones de la ESCOM-IPN
#   aprobadas por el SISS.
URL=https://serviciosocial.ipn.mx/infoServSoc/InfoServSocListaPrsttrPerf.do?cvePerfil=36
URL2=https://serviciosocial.ipn.mx/infoServSoc/InfoServSocListaProgsActivi.do?cvePerfil=36

LINKS=$( curl "$URL" | grep Ver | grep -Eo "&cvePrsttr=[0-9]+" )

for LINK in ${LINKS}
do
	curl -s "${URL2}${LINK}" | sed -E "s/subtitulo2?/fila/g" | hq ".fila" text | sed "/Regresar/d"
done;
# Dependencies:
#   - curl
#   - grep
#   - hq
#   - sed
# License:
#   serviciosocial
#   Copyright (C) 2023 Oziel Cortés Piña

#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.

#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.

#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# Contact: ozielcortespinna@gmail.com
